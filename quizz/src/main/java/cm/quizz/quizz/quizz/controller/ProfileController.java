package cm.quizz.quizz.quizz.controller;

import java.awt.print.Pageable;
import java.lang.reflect.Method;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JacksonInject.Value;

import cm.quizz.quizz.inputValue.InputValue;
import cm.quizz.quizz.inputValue.OutputReturn;
import cm.quizz.quizz.model.Registration;
import cm.quizz.quizz.repository.UserRepository;
import cm.quizz.quizz.service.ProfileService;
@Transactional
@RestController
public class ProfileController {
	@Autowired
	private ProfileService pr;
	
	@Autowired
	UserRepository ur;
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/profile/{name}" , method = RequestMethod.GET)
	public List<Registration> viewProfile(@PathVariable String name){
		
			return pr.viewProfile(name) ;
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value="/users")
	public List<String> viewProfile(){
		System.out.println("chandan");
	return pr.users();
//		
   }
	
	
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value="/userall" , method = RequestMethod.GET)
	public Iterable<Registration> viewuser(){
		
			return ur.findAll();
	}
	
	@CrossOrigin(origins ="*")
	@RequestMapping(value = "/delete/{name}",method = RequestMethod.DELETE)
	public void delete(@PathVariable String name) {
		ur.deleteByUsername(name);
	}
	
	
}
