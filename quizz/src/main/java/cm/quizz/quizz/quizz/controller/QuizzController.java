package cm.quizz.quizz.quizz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cm.quizz.quizz.service.QuizzService;

@RestController
public class QuizzController {
	@Autowired
	private QuizzService QuizzService;
	
	@CrossOrigin(origins = "*")
	@RequestMapping("/quizz/{type}")
	public String quizz(@PathVariable String type) {
		System.out.println(type);
		return QuizzService.quizz(type);
	}

}
