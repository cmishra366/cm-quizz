package cm.quizz.quizz.quizz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cm.quizz.quizz.inputValue.InputValue;
import cm.quizz.quizz.service.LoginJson;
import cm.quizz.quizz.service.LoginService;
import cm.quizz.quizz.service.RegisterService;
import cm.quizz.quizz.service.RegistrationJson;


@RestController
public class LoginController {
	
	@Autowired
	LoginService ls;
	@Autowired
	RegisterService rs;

	@CrossOrigin(origins = "*")

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public LoginJson login(@RequestBody InputValue inputValue) {

		return ls.checkUser(inputValue);

	}

	@CrossOrigin(origins = "*")
	//@RequestMapping(value = "/register", method = RequestMethod.POST)
	@RequestMapping(method = RequestMethod.POST,value = "/register")
	public RegistrationJson register(@RequestBody InputValue inputValue) {

		
		return rs.register(inputValue) ;

	}
	
	

}
