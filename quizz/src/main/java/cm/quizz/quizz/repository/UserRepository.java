package cm.quizz.quizz.repository;


import java.util.List;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cm.quizz.quizz.inputValue.OutputReturn;
import cm.quizz.quizz.model.Registration;

@Repository
public interface UserRepository extends CrudRepository<Registration, Integer>{
	List<Registration> findByUsernameAndPassword(String Username,String Password);
	List<Registration> findByEmailId(String EmailId);
    List<Registration> findByUsername(String Username);
	 void deleteByUsername(String Username);
   
   
    @Query(value="SELECT id,username,email_id FROM registration",nativeQuery = true)
    List<String> findAllUsername();
		
	

}
