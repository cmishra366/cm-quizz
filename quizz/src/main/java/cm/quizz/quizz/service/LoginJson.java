package cm.quizz.quizz.service;

public class LoginJson {
	String status;
	String EmailId;
	String token;
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEmailId() {
		return EmailId;
	}
	public void setEmailId(String emailId) {
		EmailId = emailId;
	}
	@Override
	public String toString() {
		return "LoginJson [status=" + status + ", EmailId=" + EmailId + "]";
	}

}
