package cm.quizz.quizz.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cm.quizz.quizz.inputValue.InputValue;
import cm.quizz.quizz.model.Registration;
import cm.quizz.quizz.repository.UserRepository;

@Service
public class RegisterService {
	Registration r=new Registration();
	@Autowired
	UserRepository ur;
	RegistrationJson rj = new RegistrationJson();
	boolean bemail = false;
	boolean busername = false;

	public RegistrationJson register(InputValue inputValue) {
		
		// checking email
		Iterable<Registration> i = ur.findByEmailId(inputValue.getEmailId());
		
		ArrayList<Registration> al = (ArrayList<Registration>) i;
		
		if (al.isEmpty()) {
			r.setEmailId(inputValue.getEmailId());
			bemail = true;
			rj.setEmailId(inputValue.getEmailId());
		} else {
			r.setUsername(null);
			rj.setEmailId("email already exist");
			rj.setStatus("failed");
			bemail=false;
			
		}
		
		// checking username
		Iterable<Registration> i1 = ur.findByUsername(inputValue.getUsername());
		ArrayList<Registration> al2 = (ArrayList<Registration>) i1;
		if (al2.isEmpty()) {
			r.setUsername(inputValue.getUsername());
			//interest
			r.setInterest(inputValue.getInterest());
			busername = true;
			rj.setUsername(null);
		} else {
			busername=false;
			rj.setUsername("username already exist");
			rj.setStatus("failed");
			r.setUsername(null);
		}
		
		
		
		if (bemail) {
			if (busername) {
				r.setUsername(inputValue.getUsername());
				r.setPassword(inputValue.getPassword());
				rj.setStatus("success");
				ur.save(r);
				
			}
		}
	
		
		
		

		return rj;
	}

	

}
