package cm.quizz.quizz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import cm.quizz.quizz.inputValue.InputValue;
import cm.quizz.quizz.inputValue.OutputReturn;
import cm.quizz.quizz.model.Registration;
import cm.quizz.quizz.repository.UserRepository;

@Service
public class ProfileService {

	@Autowired
	UserRepository ur;
	
	public List<Registration> viewProfile(String name) {
		System.out.println(name);
		return ur.findByUsername(name);
	}

	public List<String> users() {
		System.out.println("mishra");
		List<String> list=ur.findAllUsername();
		return list;
	}

}
